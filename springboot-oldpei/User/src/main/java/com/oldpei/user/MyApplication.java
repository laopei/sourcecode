package com.oldpei.user;

import com.oldpei.springboot.OldpeiSpringApplication;
import com.oldpei.springboot.OldpeiSpringbootApplication;

/**
 * @author OLDPEI
 */
@OldpeiSpringbootApplication
public class MyApplication {

    public static void main(String[] args) throws Exception {
        OldpeiSpringApplication.run(MyApplication.class);
    }
}
