package com.oldpei.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author OLDPEI
 */
@RestController
public class UserController {
    @GetMapping("/user")
    public String getUser(String id) {
        return "获取用户" + id;
    }
}
