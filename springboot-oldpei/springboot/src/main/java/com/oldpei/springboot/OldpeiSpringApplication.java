package com.oldpei.springboot;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.util.Map;

/**
 * @author OLDPEI
 * springboot启动类
 */
public class OldpeiSpringApplication {
    /**
     * springboot启动方法
     *
     * @param aclass
     * @param args
     */
    public static void run(Class aclass, String[] args) {
    }

    /**
     * springboot启动方法
     *
     * @param aclass
     */
    public static void run(Class aclass) throws Exception {

        //启动tomcat服务 或者其他容器
        //需要创建spring容器

        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        //注册配置类，使用上述容器时，需要配置类加入一些配置注解 和 @Bean
        //
        applicationContext.register(aclass);
        applicationContext.refresh();

        //获取webserver
        WebServer webServer = getWebServer(applicationContext);
        webServer.start();

    }

    private static WebServer getWebServer(WebApplicationContext applicationContext) throws Exception {
        Map<String, WebServer> beansOfType = applicationContext.getBeansOfType(WebServer.class);
        if (beansOfType.size() == 0) {
            throw new NullPointerException("没有提供webserver");
        }
        if (beansOfType.size() > 1) {
            throw new Exception("server数量大于2");
        }
        return beansOfType.values().stream().findFirst().get();
    }


}
