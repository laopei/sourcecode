package com.oldpei.springboot;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author OLDPEI
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
//默认配置类的扫描路径，如果不传入路径则默认扫描使用本注解的类的全包
@Import(AutoConfigurationImportSelector.class)
@ComponentScan
public @interface OldpeiSpringbootApplication {
}
