package com.oldpei.springboot;

import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author OLDPEI
 * 自动选择装配的自动配置类
 */
public class AutoConfigurationImportSelector implements DeferredImportSelector {
    private static final String[] NO_IMPORTS = {};

    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        //配置加载全部自动装配类
        //检测包括jar包在内的所有代码中META-INF/spring.factories文件，读取其中的
        //org.springframework.boot.autoconfigure.EnableAutoConfiguration=...
        //再次工程我用com.oldpei.springboot.EnableAutoConfiguration模拟代替上述key值

        return getAutoConfigurationEntry(annotationMetadata);
    }

    protected String[] getAutoConfigurationEntry(AnnotationMetadata annotationMetadata) {
        List<String> strings = new ArrayList<>();
        ClassLoader classLoader = this.getClass().getClassLoader();
        try {
            strings = SpringFactoriesLoader.loadFactoryNames(EnableAutoConfiguration.class, classLoader);
            for (String string : strings) {
                System.out.println(string);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return StringUtils.toStringArray(strings);
    }

    private static Map<String, List<String>> loadSpringFactories(ClassLoader classLoader) {
        Map<String, List<String>> result = new HashMap<>();
        try {
            Enumeration<URL> urls = classLoader.getResources("META-INF/spring.factories");
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                UrlResource resource = new UrlResource(url);
                Properties properties = PropertiesLoaderUtils.loadProperties(resource);
                for (Map.Entry<?, ?> entry : properties.entrySet()) {
                    String factoryTypeName = ((String) entry.getKey()).trim();
                    String[] factoryImplementationNames =
                            StringUtils.commaDelimitedListToStringArray((String) entry.getValue());
                    for (String factoryImplementationName : factoryImplementationNames) {
                        result.computeIfAbsent(factoryTypeName, key -> new ArrayList<>())
                                .add(factoryImplementationName.trim());
                    }
                }
            }
            result.replaceAll((factoryType, implementations) -> implementations.stream().distinct()
                    .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList)));
        } catch (IOException ex) {
            throw new IllegalArgumentException("Unable to load factories from location [" +
                    "META-INF/spring.factories" + "]", ex);
        }
        return result;
    }


}
