package com.oldpei.springboot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @author OLDPEI
 * 为webserver自动配置类
 */
@Configuration
public class WebServerAutoConfiguration {

    @Bean
    /**
     * 控制在有依赖时生效
     */
    @Conditional(JettyCondition.class)
    public JettyWebServer jettyWebServer() {
        return new JettyWebServer();
    }

    @Bean
    @Conditional(TomcatCondition.class)
    public TomcatWebServer tomcatWebServer() {
        return new TomcatWebServer();
    }
}
