package com.oldpei.mybatis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author OLDPEI
 */
public class StringTypeHandler implements TypeHandler<String> {

    /**
     * 赋值
     *
     * @param preparedStatement 预编译对象
     * @param i                 参数位置
     * @param parameter         变量值
     */
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, String parameter) throws SQLException {
        preparedStatement.setString(i, parameter);
    }

    /**
     * 获取结果集中的字段
     *
     * @param resultSet
     * @param columnName
     * @return
     */
    @Override
    public String getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getString(columnName);
    }
}
