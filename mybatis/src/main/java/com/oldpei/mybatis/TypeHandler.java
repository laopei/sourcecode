package com.oldpei.mybatis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author OLDPEI
 */
public interface TypeHandler<T> {
    /**
     * 赋值
     *
     * @param preparedStatement 预编译对象
     * @param i                 参数位置
     * @param parameter         变量值
     */
    void setParameter(PreparedStatement preparedStatement, int i, T parameter) throws SQLException;

    /**
     * 获取结果集中的字段
     *
     * @param resultSet
     * @param columnName
     * @return
     */
    T getResult(ResultSet resultSet, String columnName) throws SQLException;
}
