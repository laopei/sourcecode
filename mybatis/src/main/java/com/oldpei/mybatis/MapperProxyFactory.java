package com.oldpei.mybatis;

import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

/**
 * @author OLDPEI
 * 代理工厂，用于代理mapper接口，创建实例，调用方法
 */
public class MapperProxyFactory {

    private static Map<Class, TypeHandler> typeHandlerMap = new HashMap<>();

    static {
        typeHandlerMap.put(String.class, new StringTypeHandler());
        typeHandlerMap.put(Integer.class, new IntegerTypeHandler());
        typeHandlerMap.put(int.class, new IntegerTypeHandler());

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static <T> T getMapper(Class<T> mapper) {
        Object o = Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class[]{mapper}, (proxy, method, args) -> {

            Select annotation = method.getAnnotation(Select.class);
            if (annotation == null) {
                throw new Exception("没有指定sql");
            }
            //解析sql
            String sql = annotation.value();
            if (sql.equals("")) {
                throw new Exception("指定sql为空");
            }

            System.out.println("传入sql： " + sql);
            Map<String, Object> paramValueMap = new HashMap<>();
            //解析出 参数名：参数值 形成map
            Parameter[] parameters = method.getParameters();
            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];
                //循环参数的位置和入参args位置一致
                paramValueMap.put(parameter.getName(), args[i]);
                paramValueMap.put(parameter.getAnnotation(Param.class).value(), args[i]);
            }

            //sql中参数位置对应替换，将sql中#{} 变量key值，按照顺序存入list
            ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler();
            GenericTokenParser parser = new GenericTokenParser("#{", "}", handler);
            //使用关键字符"#{" 和 "}" 作为起始和结束的标识，将其中的字符传用“?”替换并且将其中的字符保存至handler中的lisst中
            String parSql = parser.parse(sql);

            System.out.println("替换后的sql：" + parSql);
            List<ParameterMapping> parameterMappings = handler.getParameterMappings();

            //1.jdbc链接
            Connection connection = getConnection();
            //2.预编译
            PreparedStatement preparedStatement = connection.prepareStatement(parSql);

            //根据方法传入的参数给占位符赋值
            TypeVariable<Method>[] typeParameters = method.getTypeParameters();
            for (int i = 0; i < parameterMappings.size(); i++) {
                String property = parameterMappings.get(i).getProperty();//变量名
                Object value = paramValueMap.get(property);
                //TypeHandler根据不同的类型采用不同的赋值方式
                Class<?> aClass = value.getClass();
                TypeHandler typeHandler = typeHandlerMap.get(aClass);
                typeHandler.setParameter(preparedStatement, i + 1, value);
                //或者直接采用setObject
                //preparedStatement.setObject(i + 1, value);
            }
            System.out.println(preparedStatement.toString());

            //3.执行sql
            preparedStatement.execute();
            //4.封装结果
            //获取返回类型
            Class returnClass = null;
            Type genericReturnType = method.getGenericReturnType();
            if (genericReturnType instanceof Class) {
                //返回类型不存在泛型
                returnClass = (Class) genericReturnType;
            } else if (genericReturnType instanceof ParameterizedType) {
                //返回类型存在泛型
                Type[] actualReturnType = ((ParameterizedType) genericReturnType).getActualTypeArguments();
                returnClass = (Class) actualReturnType[0];
            }
            List<Object> list = new ArrayList<>();
            //获取返回值变量名的集合
            ResultSet resultSet = preparedStatement.getResultSet();
            ResultSetMetaData metaData = resultSet.getMetaData();
            List<String> columnNames = new ArrayList<>();
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                columnNames.add(metaData.getColumnName(i + 1));
            }
            //获取对象类的set方法集合
            Map<String, Method> setterMethondMap = new HashMap<>();
            for (Method declaredMethod : returnClass.getDeclaredMethods()) {
                if (declaredMethod.getName().startsWith("set")) {
                    String propertyName = declaredMethod.getName().substring(3);
                    propertyName = propertyName.substring(0, 1).toLowerCase(Locale.ROOT) + propertyName.substring(1);
                    setterMethondMap.put(propertyName, declaredMethod);
                }
            }

            while (resultSet.next()) {
                Object instance = returnClass.getDeclaredConstructor().newInstance();
                for (int i = 0; i < columnNames.size(); i++) {
                    String columnName = columnNames.get(i);
                    Method setMethod = setterMethondMap.get(columnName);
                    Class<?>[] parameterTypes = setMethod.getParameterTypes();
                    setMethod.invoke(instance, typeHandlerMap.get(parameterTypes[0]).getResult(resultSet, columnName));
                }
                list.add(instance);
            }
            Object returnObject;
            if (method.getReturnType().equals(List.class)) {
                returnObject = list;
            } else {
                if (list.size() > 0) {
                    returnObject = list.get(0);
                } else {
                    returnObject = null;
                }
            }
            preparedStatement.close();
            connection.close();
            return returnObject;
        });
        return (T) o;
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/mysql?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8", "root", "123456"
        );
        return connection;
    }
}
