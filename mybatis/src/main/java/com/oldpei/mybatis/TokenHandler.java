package com.oldpei.mybatis;

/**
 * @author OLDPEI
 */
public interface TokenHandler {
    String handleToken(String var1);
}
