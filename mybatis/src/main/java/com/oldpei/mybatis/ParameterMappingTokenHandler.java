package com.oldpei.mybatis;

import java.util.ArrayList;
import java.util.List;

/**
 * @author OLDPEI
 */
public class ParameterMappingTokenHandler implements TokenHandler {
    private final List<ParameterMapping> parameterMappings = new ArrayList();

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    @Override
    public String handleToken(String content) {
        this.parameterMappings.add(new ParameterMapping(content));
        return "?";
    }
}
