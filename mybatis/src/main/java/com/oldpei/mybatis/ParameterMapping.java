//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.oldpei.mybatis;


/**
 * @author OLDPEI
 */
public class ParameterMapping {
    private String property;

    public ParameterMapping(String property) {
        this.property = property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getProperty() {
        return this.property;
    }


}
