package com.oldpei.mybatis.mytest.mapper;

import com.oldpei.mybatis.Param;
import com.oldpei.mybatis.Select;
import com.oldpei.mybatis.mytest.model.User;

import java.util.List;

/**
 * @author OLDPEI
 */
public interface UserMapper {
    @Select("select * from t_user where name= #{name} and age = #{age} ")
    List<User> getList(@Param("name") String name, @Param("age") Integer age);

    @Select("select * from t_user where id = #{id}")
    User getUserById(@Param("id") int id);
}
