package com.oldpei.mybatis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author OLDPEI
 */
public class IntegerTypeHandler implements TypeHandler<Integer> {
    /**
     * 赋值
     *
     * @param preparedStatement 预编译对象
     * @param i                 参数位置
     * @param parameter         变量值
     */
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, Integer parameter) throws SQLException {
        preparedStatement.setInt(i, parameter);
    }

    /**
     * 获取结果集中的字段
     *
     * @param resultSet
     * @param columnName
     * @return
     */
    @Override
    public Integer getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getInt(columnName);
    }
}
