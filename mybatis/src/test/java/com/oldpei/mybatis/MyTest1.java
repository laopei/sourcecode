package com.oldpei.mybatis;

public class MyTest1 {

    public String reverseWords(String str) {
        // write code here
        String[] arry = str.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String s : arry) {
            sb.append(reverseWord(s)  );
            sb.append(" ");
        }
        return sb.substring(0, sb.length() - 1);
    }

    /**
     * 翻转字符串
     *
     * @param word
     * @return
     */
    private String reverseWord(String word) {
        if (word.length() <= 1) {
            return word;
        } else {
            return reverseWord(word.substring(1)) + word.charAt(0);
        }
    }

    public static void main(String[] args) {
        System.out.println(new MyTest1().reverseWords("i am a man"));
    }
}
