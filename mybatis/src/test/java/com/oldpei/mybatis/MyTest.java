package com.oldpei.mybatis;

import com.oldpei.mybatis.mytest.mapper.UserMapper;
import com.oldpei.mybatis.mytest.model.User;

import java.util.List;

public class MyTest {
    public static void main(String[] args) {
        UserMapper userMapper = MapperProxyFactory.getMapper(UserMapper.class);
        List<User> list = userMapper.getList("张三", 17);
        User u=userMapper.getUserById(1);
        System.out.println(u);
    }
}
