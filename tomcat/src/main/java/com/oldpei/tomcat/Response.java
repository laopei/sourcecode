package com.oldpei.tomcat;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 请求响应
 *
 * @author OLDPEI
 */
public class Response extends AbstractHttpServletResponse {
    private int status = 200;
    private String message = "ok";
    private Map<String, Object> headers = new HashMap<>();
    private Request request;
    private OutputStream socketOutputStream;
    private ResponseServletOutputStream servletOutputStream = new ResponseServletOutputStream();

    public static final byte SP = ' ';
    public static final byte CR = '\r';
    public static final byte LF = '\n';

    public Response(Request request) {
        this.request = request;
        try {
            this.socketOutputStream = request.getSocket().getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addHeader(String s, String s1) {
        headers.put(s, s1);
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int i) {
        status = i;
    }

    @Override
    public ResponseServletOutputStream getOutputStream() throws IOException {
        return servletOutputStream;
    }

    public void complete() {
        try {
            sendResponseLine();
            sendResponseHeader();
            sendResponseBody();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 发送响应行
     */
    private void sendResponseLine() throws IOException {
        socketOutputStream.write(request.getProtocol().getBytes());
        socketOutputStream.write(SP);
        socketOutputStream.write(status);
        socketOutputStream.write(SP);
        socketOutputStream.write(message.getBytes());
        socketOutputStream.write(CR);
        socketOutputStream.write(LF);
    }

    /**
     * 发送响应头
     */
    private void sendResponseHeader() throws IOException {

        if (!headers.containsKey("Content-Length")) {
            headers.put("Content-Length", String.valueOf(servletOutputStream.getPos()));
        }
        if (!headers.containsKey("Content-Type")) {
            headers.put("Content-Type", "text/plain;charset=utf-8");
        }
        for (Map.Entry<String, Object> entry : headers.entrySet()) {
            String key = entry.getKey();
            String value = (String) entry.getValue();
            socketOutputStream.write(key.getBytes());
            socketOutputStream.write(":".getBytes());
            socketOutputStream.write(value.getBytes());
            socketOutputStream.write(CR);
            socketOutputStream.write(LF);
        }

        socketOutputStream.write(CR);
        socketOutputStream.write(LF);
    }

    /**
     * 发送响应体
     */
    private void sendResponseBody() throws IOException {


        socketOutputStream.write(getOutputStream().getBytes());
    }


}
