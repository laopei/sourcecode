package com.oldpei.tomcat;

import javax.servlet.Servlet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author OLDPEI
 * tomcat中的应用类
 */
public class Context {
    private String appName;
    private Map<String, Servlet> urlPatterns = new HashMap<>();

    public Context(String appName) {
        this.appName = appName;
    }

    public Map<String, Servlet> getUrlPatterns() {
        return urlPatterns;
    }

    public Servlet getUrlPattern(String url) {
        return urlPatterns.get(url);
    }

    public void setUrlPatterns(Map<String, Servlet> urlPatterns) {
        this.urlPatterns = urlPatterns;
    }

    public void addUrlPattern(String key, Servlet servlet) {
        urlPatterns.put(key, servlet);
    }
}
