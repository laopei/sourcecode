package com.oldpei.tomcat;

import javax.servlet.Servlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author OLDPEI
 */
public class Tomcat {
    private Map<String, Context> contextMap = new HashMap<>();

    public void start() {
        //Socket链接   协议是TCP
        //BIO方式
        try {
            //异步等待连接

            ExecutorService executorService =
                    new ThreadPoolExecutor(20, 20,
                            6000L, TimeUnit.MILLISECONDS,
                            new LinkedBlockingQueue<>(), Executors.defaultThreadFactory());

            ServerSocket server = new ServerSocket(8080);

            //阻塞的等待访问
            while (true) {
                Socket socket = server.accept();
                executorService.submit(new SocketProcess(socket, this));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 处理访问进来的请求
     *
     * @param socket
     */
    private void processSocket(Socket socket) {
    }

    public static void main(String[] args) {
        Tomcat tomcat = new Tomcat();
        tomcat.deployApps();
        System.out.println("Tomcat加载app成功");
        for (String key : tomcat.contextMap.keySet()) {
            System.out.println("加载了以下系统：" + key);
        }
        tomcat.start();
    }

    /**
     * 批量部署应用的方法
     */
    private void deployApps() {
        //System.getProperty("user.dir")获取的是最外层项目目录，所以需要指定子项目
        File webapps = new File(System.getProperty("user.dir"), "tomcat/webapps");
        for (String app : webapps.list()) {
            Context appContext = new Context(app);
            deployApp(webapps, app, appContext);
        }
    }

    /**
     * 部署指定应用的方法
     *
     * @param webapps
     * @param appName
     */
    private void deployApp(File webapps, String appName, Context appContext) {
        //首先遍历class 获取servlet
        File appDir = new File(webapps, appName);
        File classesDir = new File(appDir, "classes");
        //获取应用目录下的全部文件
        List<File> allFiles = getAllFilePath(classesDir);
        allFiles.forEach(file -> {
            //已经获取到类文件，需要使用类加载器加载后判断类是否实现了httpservlet
            try {
                //文件名替换为类的全包名后进行加载
                String name = file.getPath();
                name = name.replace(classesDir.getPath() + "\\", "");
                name = name.replace(".class", "");
                name = name.replace("\\", ".");
                System.out.println("已加载类：" + name);
                //获取加载的类,使用自定义的类加载器加载webapps目录下的文件
                WebappClassLoader classLoader = new WebappClassLoader(new URL[]{classesDir.toURI().toURL()});
//                Class<?> aClass = Thread.currentThread().getContextClassLoader().loadClass(name);
                Class<?> aClass = classLoader.loadClass(name);
                if (aClass != null && HttpServlet.class.isAssignableFrom(aClass)) {
                    //判断加载的类确实是继承了HttpServlet类的servlet
                    System.out.println("判断类【" + aClass + "】是servlet");
                    //解析servelet中存在WebServlet的注解，获取注解url
                    WebServlet webServlet = aClass.getAnnotation(WebServlet.class);
                    if (webServlet != null) {
                        String[] urlPatterns = webServlet.urlPatterns();
                        for (String urlPattern : urlPatterns) {
                            //将servelet加载到tomcat的contxt对象中
                            appContext.addUrlPattern(urlPattern.substring(1), (Servlet) aClass.getDeclaredConstructor().newInstance());
                        }

                    }


                }
            } catch (ClassNotFoundException | MalformedURLException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

            contextMap.put(appName, appContext);

        });
    }

    /**
     * 地柜获取目录下全部文件
     *
     * @param srcPath
     * @return
     */
    public List<File> getAllFilePath(File srcPath) {
        List<File> result = new ArrayList<>();
        File[] files = srcPath.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isDirectory()) {
                    result.addAll(getAllFilePath(file));
                } else {
                    result.add(file);
                }
            }
        }

        return result;
    }

    public Map<String, Context> getContextMap() {
        return contextMap;
    }
}
