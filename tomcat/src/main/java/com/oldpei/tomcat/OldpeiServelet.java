package com.oldpei.tomcat;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author OLDPEI
 * servelet处理，应在应用中
 */
@WebServlet(urlPatterns = "/oldpei")
public class OldpeiServelet extends HttpServlet {
    /**
     * 处理get请求
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            System.out.println(req.getMethod());
            String result = "Hello World";
//            resp.addHeader("Content-Length", String.valueOf(result.length()));
//            resp.addHeader("Content-Type", "text/plain;charset=utf-8");

            ServletOutputStream outputStream = resp.getOutputStream();
            outputStream.write(result.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
