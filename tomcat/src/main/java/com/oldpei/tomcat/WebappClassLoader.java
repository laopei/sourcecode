package com.oldpei.tomcat;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author OLDPEI
 * 自定义的类加载器
 */
public class WebappClassLoader extends URLClassLoader {

    public WebappClassLoader(URL[] urls) {
        super(urls);
    }
}
