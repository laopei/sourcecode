package com.oldpei.tomcat;

import javax.servlet.Servlet;
import java.io.InputStream;
import java.net.Socket;

/**
 * @author OLDPEI
 */
public class SocketProcess implements Runnable {
    private Socket socket;
    private Tomcat tomcat;

    SocketProcess(Socket socket, Tomcat tomcat) {
        this.socket = socket;
        this.tomcat = tomcat;

    }


    @Override
    public void run() {
        processSocket();
    }

    private void processSocket() {
        //处理socket连接 ，获取的数据为字节流，需要按照http请求的格式读取
        try {
            InputStream inputStream = socket.getInputStream();
            byte[] bytes = new byte[1024];
            //读取1kb的数据进入bytes,此处配置了可以读取的最长请求
            inputStream.read(bytes);

//            System.out.println(new String(bytes));

            //读数据
            //解析字节流 获取请求类型
            int pos = 0;
            int begin = 0, end = 0;
            for (; pos < bytes.length; pos++, end++) {
                if (bytes[pos] == ' ') {
                    break;
                }
            }
            //组合空格之前的字节流，转换成字符串即为请求类型
            StringBuilder method = new StringBuilder();
            for (; begin < end; begin++) {
                method.append((char) bytes[begin]);
            }
            System.out.println("请求类型为：" + method);


            //解析请求地址
            pos++;
            begin++;
            end++;
            for (; pos < bytes.length; pos++, end++) {
                if (bytes[pos] == ' ') {
                    break;
                }
            }
            //组合空格之前的字节流，转换成字符串即为请求地址
            StringBuilder url = new StringBuilder();
            for (; begin < end; begin++) {
                url.append((char) bytes[begin]);
            }
            System.out.println("请求地址为：" + url);

            //解析协议版本
            pos++;
            begin++;
            end++;
            for (; pos < bytes.length; pos++, end++) {
                if (bytes[pos] == '\r' && bytes[pos + 1] == '\n') {
                    break;
                }
            }
            //组合空格之前的字节流，转换成字符串即为请求地址
            StringBuilder protocl = new StringBuilder();
            for (; begin < end; begin++) {
                protocl.append((char) bytes[begin]);
            }
            System.out.println("协议版本为：" + protocl);
            //组装解析后的请求
            //构造实现HttpServletRequest 和 HttpServletResponse 的对象传入方法
            Request request = new Request(method.toString(), url.toString(), protocl.toString(), socket);
            Response response = new Response(request);

            //根据请求的url的第一段 匹配tomcat的content对象的
            String requestUrl = request.getRequestURI();
            requestUrl = requestUrl.substring(1);
            String[] parts = requestUrl.split("/");
            String appName = parts[0];
            String part;
            if (parts != null && parts.length > 1) {
                part = parts[1];
            } else {
                part = appName;
            }
            //获取context对象名

            Context context = tomcat.getContextMap().get(appName);
            Servlet servlet;
            if (context != null) {
                servlet = context.getUrlPattern(part);
            } else {
                servlet = new DefaultServlet();
            }

            //已经将响应存入Response中的ServletOutputStream  实现类 ResponseServletOutputStream
            servlet.service(request, response);

            //调用结束方法
            response.complete();

        } catch (
                Exception e) {
            e.printStackTrace();
        }


    }
}
