package com.oldpei.tomcat;

import javax.servlet.ServletOutputStream;
import java.io.IOException;

/**
 * @author OLDPEI
 * servelet响应返回体
 */
public class ResponseServletOutputStream extends ServletOutputStream {
    //需要考虑自动扩容
    private byte[] bytes = new byte[1024];
    /**
     * 下标
     */
    private int pos = 0;

    @Override
    public void write(int b) throws IOException {
        bytes[pos++] = (byte) b;
    }

    public byte[] getBytes() {
        return bytes;
    }


    public int getPos() {
        return pos;
    }


}
