package com.oldpei.tomcat;

import javax.servlet.ServletOutputStream;
import java.io.IOException;

/**
 * @author OLDPEI
 */
public class ResponseOutputStream extends ServletOutputStream {
    private byte[] bytes = new byte[1024];
    private int pos;

    @Override
    public void write(int b) throws IOException {
        //将响应体等参数写入输出流
        bytes[pos] = (byte) b;
        pos++;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public int getPos() {
        return pos;
    }
}
