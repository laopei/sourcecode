package site.oldpei.convertor.utils.convertor;

import lombok.AllArgsConstructor;
import lombok.Data;
import site.oldpei.convertor.function.SBiConsumer;
import site.oldpei.convertor.function.SFunction;

@Data
@AllArgsConstructor
public class DictConvertor<T> {
    SFunction<T, ?> getDictCodeValue;
    SBiConsumer<T, String> setDictCodeName;
    String dictCodeType;

}
