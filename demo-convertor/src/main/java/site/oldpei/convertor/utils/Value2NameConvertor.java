package site.oldpei.convertor.utils;

import site.oldpei.convertor.function.SBiConsumer;
import site.oldpei.convertor.function.SFunction;
import site.oldpei.convertor.service.Value2NameConvertorBS;
import site.oldpei.convertor.utils.convertor.DictConvertor;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典码值转换器
 */
public class Value2NameConvertor<T> {
    private List<DictConvertor<T>> dictConvertorList = new ArrayList<>();


    public Value2NameConvertor<T> value2Name(SFunction<T, ?> getDictCodeValue, SBiConsumer<T, String> setDicCodeName, String dictCodeType) {
        dictConvertorList.add(new DictConvertor<T>(getDictCodeValue, setDicCodeName, dictCodeType));
        return this;
    }

    public void convert(List<T> rows) {
        if (!(dictConvertorList == null) && !(dictConvertorList.isEmpty())) {
            Value2NameConvertorBS value2NameConvertorBS = SpringContextUtils.getBean(Value2NameConvertorBS.class);
            dictConvertorList.forEach(
                    dictConvertor -> {
                        value2NameConvertorBS.setDictName(rows, dictConvertorList);
                    }
            );
        }
    }
}
