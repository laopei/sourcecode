package site.oldpei.convertor.function;

import java.io.Serializable;
import java.util.function.BiConsumer;

@FunctionalInterface
public interface SBiConsumer<T, U> extends BiConsumer<T, U>, Serializable {
}
