package site.oldpei.convertor.service;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import site.oldpei.convertor.utils.convertor.DictConvertor;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;

@Service
public class Value2NameConvertorBS {
    public <T> void setDictName(List<T> rows, List<DictConvertor<T>> dictConvertorList) {
        if (dictConvertorList == null || dictConvertorList.isEmpty()) {
            return;
        }
        //获取字典码
        Set<String> dictCodes = new HashSet<>();
        dictConvertorList.forEach(dictConvertor -> {
            dictCodes.add(dictConvertor.getDictCodeType());
        });

        //数据库查询，根据字典码获取字典值
        Map<String, Map<String, String>> dictMaps = new HashMap();

        rows.forEach(row -> {
            dictConvertorList.forEach(
                    dictConvertor -> {
                        Function<T, ?> getDictCodeValue = dictConvertor.getGetDictCodeValue();
                        String dictCodeType = dictConvertor.getDictCodeType();
                        BiConsumer<T, String> setDictCodeName = dictConvertor.getSetDictCodeName();
                        Map<String, String> codeMap = dictMaps.get(dictCodeType);
                        Assert.notNull(codeMap, "字典码值不存在，codeType:" + dictCodeType);
                        //获取get方法返回值
                        String code = (String) getDictCodeValue.apply(row);
                        String name = codeMap.get(code);
                        setDictCodeName.accept(row, name);
                    }
            );
        });
    }
}
