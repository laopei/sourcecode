package com.oldpei;

import com.oldpei.common.URL;
import com.oldpei.protocol.HttpServer;
import com.oldpei.register.LocalRegister;
import com.oldpei.register.MapRemoteRegister;

/**
 * @author OLDPEI
 */
public class Provider {
    public static void main(String[] args) {
        //Netty  , Tomcat , Socket
        //rpc框架最好可以选择网络框架
        //先本地注册服务
        LocalRegister.regist(HelloService.class.getName(), HelloServiceImpl.class);


        //远程注册中心注册
        URL u = new URL("localhost", 8080);
        MapRemoteRegister.regist(HelloService.class.getName(), u);

        //再启动tomcat
        HttpServer server = new HttpServer();
        server.start(u.getHostname(), u.getPort());
    }
}
