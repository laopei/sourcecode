package com.oldpei;

import com.oldpei.proxy.ProxyFactory;

public class Consumer {
    public static void main(String[] args) {
//
//        //1、实现思路如下
//        //先根据调用接口和方法等参数构建调用对象·
//        Invocation invocation = new Invocation(HelloService.class.getName(), "sayHello", new Class[]{String.class},
//                new Object[]{"oldpei"});
//
//        HttpClient client = new HttpClient();
//        //利用httpclient发送http请求至httpserver
//        String value = client.send("localhost", 8080, invocation);
//        System.out.println(value);
//
        //2、根据上述思路，通过代理对象的方法，直接将需要调用的接口通过反射位方法赋值，将上述逻辑赋值入方法
        HelloService service = ProxyFactory.getProxy(HelloService.class);
        String result = service.sayHello("oldpei");
        System.out.println(result);


    }
}
