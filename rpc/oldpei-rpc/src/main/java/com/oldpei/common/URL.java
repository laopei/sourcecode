package com.oldpei.common;

import java.io.Serializable;

/**
 * @author OLDPEI
 * url对象
 */
public class URL implements Serializable {
    public URL(String hostname, Integer port) {
        this.hostname = hostname;
        this.port = port;
    }

    String hostname;
    Integer port;

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "URL{" +
                "hostname='" + hostname + '\'' +
                ", port=" + port +
                '}';
    }
}
