package com.oldpei.loadbalance;

import com.oldpei.common.URL;

import java.util.List;
import java.util.Random;

/**
 * @author OLDPEI
 * 负载均衡
 */
public class LoadBalance {
    //随机算法
    public static URL random(List<URL> urls) {
        Random random = new Random();
        int i = random.nextInt(urls.size());
        return urls.get(i);
    }
}
