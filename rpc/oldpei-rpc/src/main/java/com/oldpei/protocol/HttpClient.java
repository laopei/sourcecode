package com.oldpei.protocol;

import com.oldpei.common.Invocation;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author OLDPEI
 * http客户端
 */
public class HttpClient {

    public String send(String hostname, Integer port, Invocation invocation) throws Exception {
        //此处和server处同理可以使用配置决定服务器类型
        URL url = new URL("http", hostname, port, "/");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        //配置发送参数流
        OutputStream outputStream = connection.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(invocation);
        objectOutputStream.flush();
        objectOutputStream.close();

        //获取返回值结果
        InputStream inputStream = connection.getInputStream();
        return IOUtils.toString(inputStream, "UTF-8");
    }
}
