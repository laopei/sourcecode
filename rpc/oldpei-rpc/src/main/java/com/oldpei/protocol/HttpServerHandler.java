package com.oldpei.protocol;

import com.oldpei.common.Invocation;
import com.oldpei.register.LocalRegister;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author OLDPEI
 * http请求处理器
 */
public class HttpServerHandler {
    public void handle(HttpServletRequest req, HttpServletResponse resp) {
        // 处理请求 --> 接口、方法、方法参数
        //反序列化，从流中获取入参信息 --> 方法对象invocation
        try {
            Invocation invocation = (Invocation) new ObjectInputStream(req.getInputStream()).readObject();
            String interfaceName = invocation.getInterfaceName();
            //根据注册结果获取处理类（httpserver启动时级应该注册）
            Class implClass = LocalRegister.get(interfaceName);
            //获取方法
            Method method = implClass.getMethod(invocation.getMethodName(), invocation.getParameterTypes());
            //调用方法
            String result = (String) method.invoke(implClass.getDeclaredConstructor().newInstance(), invocation.getParameters());
            IOUtils.write(result, resp.getOutputStream(), "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
