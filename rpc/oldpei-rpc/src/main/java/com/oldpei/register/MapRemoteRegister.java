package com.oldpei.register;

import com.oldpei.common.URL;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author OLDPEI
 * 远程注册中心三要素：多个线程够可以同时访问、心跳机制判活并保证map中的值是正确的、为节约网络开销需要有本地缓存
 * 通畅可以采用redis、nacos、zookepper实现
 * 本文件中简单通过文件共享实现
 */
public class MapRemoteRegister {
    private static ConcurrentMap<String, List<URL>> map = new ConcurrentHashMap<>();


    public static void regist(String interfaceName, URL url) {
        regist(interfaceName, url, "1.0");
    }

    public static List<URL> get(String interfaceName) {
        return get(interfaceName, "1.0");
    }

    public static void regist(String interfaceName, URL url, String version) {
        String key = interfaceName + version;
        List<URL> list = map.get(key);
        if (list == null) {
            synchronized (MapRemoteRegister.class) {
                list = map.get(interfaceName);
                if (list == null) {
                    list = new ArrayList<>();
                }
            }
        }
        list.add(url);
        map.put(key, list);
        saveFile();
    }

    public static List<URL> get(String interfaceName, String version) {
        map = getFile();
        return map.get(interfaceName + version);
    }

    private static void saveFile() {
        try (FileOutputStream fos = new FileOutputStream("./tmp.txt");
             ObjectOutputStream oos = new ObjectOutputStream(fos);) {
            oos.writeObject(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ConcurrentMap<String, List<URL>> getFile() {
        try (FileInputStream fis = new FileInputStream("./tmp.txt");
             ObjectInputStream ois = new ObjectInputStream(fis);) {
            return (ConcurrentMap<String, List<URL>>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
