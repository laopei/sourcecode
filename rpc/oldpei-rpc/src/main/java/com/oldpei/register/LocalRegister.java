package com.oldpei.register;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author OLDPEI
 * 本地注册
 */
public class LocalRegister {
    private static ConcurrentHashMap<String, Class> map = new ConcurrentHashMap<>();

    public static void regist(String interfaceName, Class implClass) {
        regist(interfaceName, implClass, "1.0");
    }

    public static Class get(String interfaceName) {
        return get(interfaceName, "1.0");
    }

    public static void regist(String interfaceName, Class implClass, String version) {
        map.put(interfaceName + version, implClass);
    }

    public static Class get(String interfaceName, String version) {
        return map.get(interfaceName + version);
    }
}
