package com.oldpei.proxy;

import com.oldpei.common.Invocation;
import com.oldpei.common.URL;
import com.oldpei.loadbalance.LoadBalance;
import com.oldpei.protocol.HttpClient;
import com.oldpei.register.MapRemoteRegister;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * @author OLDPEI
 * 代理对象生成工厂
 */
public class ProxyFactory {
    public static <T> T getProxy(Class interfaceClass) {
        //读取用户配置

        Object o = Proxy.newProxyInstance(interfaceClass.getClassLoader(), new Class[]{interfaceClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Invocation invocation = new Invocation(interfaceClass.getName(), method.getName(), method.getParameterTypes(),
                        args);
                HttpClient client = new HttpClient();

                //可以通过配置mock值来实现服务mock 在jvm参数中加入 -Dmock=return:3.1415
                String mock = System.getProperty("mock");
                if (mock != null && mock.startsWith("return:")) {
                    return mock.replace("return:", "");
                }
                //服务发现
                List<URL> urls = MapRemoteRegister.get(interfaceClass.getName());
                //负载均衡去访问多个服务
                URL url = LoadBalance.random(urls);
                String value = null;
                int max = 3;//重试次数
                while (max > 0) {
                    max--;
                    try {
                        value = client.send(url.getHostname(), url.getPort(), invocation);
                    } catch (Exception e) {
                        if (max == 0) {
                            //进行服务调用时的容错逻辑
                            //并且返回值
                            return "报错了";
                        }
                        //如果没有进入上面逻辑，及证明还在重试逻辑


                    }
                }

                return value;
            }
        });
        return (T) o;
    }
}
