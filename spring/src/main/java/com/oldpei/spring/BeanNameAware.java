package com.oldpei.spring;

/**
 * @author OLDPEI
 * bean name回调方法
 */
public interface BeanNameAware {
    /**
     * @param beanName
     */
    void setBeanName(String beanName);
}
