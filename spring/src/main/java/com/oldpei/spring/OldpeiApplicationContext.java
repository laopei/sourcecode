package com.oldpei.spring;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author OLDPEI
 * bean容器实现
 */
public class OldpeiApplicationContext {

    private Class configClass;
    /**
     * 单例Bean池
     */
    private ConcurrentHashMap<String, Object> singletonObjects = new ConcurrentHashMap<>();
    /**
     * 类定义map
     */
    private ConcurrentHashMap<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>();

    /**
     * bean的后置处理器list，线程安全
     */
    private List<BeanPostProcessor> beanPostProcessorList = Collections.synchronizedList(new ArrayList<>());

    public OldpeiApplicationContext(Class configClass) {
        this.configClass = configClass;

        //加载配置文件
        ComponentScan componentScan = (ComponentScan) configClass.getAnnotation(ComponentScan.class);

        //解析ComponentScan注解，拿到扫描路径
        String path = componentScan.value();


        //类加载器 Bootstrap  ---> jre/lib
        //        Ext       ---> jre/ext/lib
        //        App       ---> classpath

        //扫描路径，通过类加载器获取当前包
        scan(path);

        //把扫描结果中的单例bean创建
        for (String beanName : beanDefinitionMap.keySet()) {
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            if (beanDefinition.getScope().equals("singleton")) {
                //单例bean
                Object bean = createBean(beanName, beanDefinition);
                singletonObjects.put(beanName, bean);
            }

        }
    }

    private Object createBean(String beanName, BeanDefinition beanDefinition) {
        Class<?> clazz = beanDefinition.getClazz();
        Object o = null;
        try {
            //反射获取无参的构造方法
            o = clazz.getDeclaredConstructor().newInstance();
            //依赖注入给Autowired注解
            for (Field declaredField : clazz.getDeclaredFields()) {
                if (declaredField.isAnnotationPresent(Autowired.class)) {
                    //判断对应参数上是否存在Autowired
                    Object bean = getBean(declaredField.getName());
                    if (bean == null) {
                        throw new Exception("无法生成bean");

                    }
                    //设置允许反射赋值
                    declaredField.setAccessible(true);
                    declaredField.set(o, bean);
                }
            }
            //自带bean name的 Aware回调方法
            if (o instanceof BeanNameAware) {
                //创建bean后判断如果实现了BeanNameAware接口后会自动回调
                ((BeanNameAware) o).setBeanName(beanName);
            }
            //后置处理器--初始化前操作
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                o = beanPostProcessor.postProcessBeforeInitialization(o, beanName);
            }
            //bean创建后的初始化
            if (o instanceof InitializingBean) {
                //创建bean后判断如果实现了InitializingBean接口后会自动回调afterPropertiesSet初始化方法
                ((InitializingBean) o).afterPropertiesSet();
            }
            //后置处理器--初始化后操作
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                o = beanPostProcessor.postProcessAfterInitialization(o, beanName);
            }
            //BeanPostProcessor bean的后置处理器

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;
    }

    private void scan(String path) {
        ClassLoader classLoader = OldpeiApplicationContext.class.getClassLoader();
        path = path.replace(".", "/");
        URL resource = classLoader.getResource(path);
        File file = new File(resource.getFile());
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files == null) {
                return;
            }
            for (File f : files) {
                String fileName = f.getAbsolutePath();
                String className = fileName.substring(fileName.indexOf("com"), fileName.lastIndexOf(".class"));
                className = className.replace("\\", ".");
                Class<?> clazz;
                try {
                    clazz = classLoader.loadClass(className);
                    if (clazz.isAnnotationPresent(Component.class)) {
                        //确认类上含有Component注解
                        //判断类是否实现了BeanPostProcessor
                        if (BeanPostProcessor.class.isAssignableFrom(clazz)) {
                            BeanPostProcessor instance = (BeanPostProcessor) clazz.getDeclaredConstructor().newInstance();
                            beanPostProcessorList.add(instance);
                        }


                        //解析类判断当前类是单例bean还是prototype（原型）bean
                        Component component = clazz.getDeclaredAnnotation(Component.class);
                        String beanName = component.value();
                        if (beanName.equals("")) {
                            beanName = clazz.getSimpleName();
                        }
                        BeanDefinition beanDefinition = new BeanDefinition();
                        if (clazz.isAnnotationPresent(Scope.class)) {
                            Scope scope = clazz.getAnnotation(Scope.class);
                            beanDefinition.setScope(scope.value());
                        } else {
                            //单例bean
                            beanDefinition.setScope("singleton");
                        }
                        beanDefinition.setClazz(clazz);
                        //组装类定义Map
                        beanDefinitionMap.put(beanName, beanDefinition);
                        //根据类生成bean
                    }
                } catch (ClassNotFoundException | NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    /**
     * 根据bean名字获取bean对象
     *
     * @param clazz
     * @return
     */
    public Object getBean(Class clazz) throws Exception {
        return null;
    }

    /**
     * 根据bean名字获取bean对象
     *
     * @param beanName
     * @return
     */
    public Object getBean(String beanName) throws Exception {
        if (!beanDefinitionMap.containsKey(beanName)) {
            throw new Exception("不存在bean");
        }
        BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        String scope = beanDefinition.getScope();
        Object o;
        if ("singleton".equals(scope)) {
            o = singletonObjects.get(beanName);
        } else {
            //每次创建新对象
            o = createBean(beanName, beanDefinition);
        }
        return o;
    }
}
