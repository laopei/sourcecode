package com.oldpei.spring;

/**
 * @author OLDPEI
 * bean创建时的后置处理器
 */
public interface BeanPostProcessor {
    /**
     * 初始化之前操作
     * @param bean
     * @param beanName
     * @return
     */
    Object postProcessBeforeInitialization(Object bean, String beanName);

    /**
     * 初始化之后操作
     * @param bean
     * @param beanName
     * @return
     */
    Object postProcessAfterInitialization(Object bean, String beanName);

}
