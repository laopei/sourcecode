package com.oldpei.spring;

/**
 * @author OLDPEI
 */
public interface InitializingBean {
    /**
     * @throws Exception
     */
    void afterPropertiesSet() throws Exception;
}
