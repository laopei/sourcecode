package com.oldpei.spring.service;

import com.oldpei.spring.Component;
import com.oldpei.spring.Scope;

/**
 * @author OLDPEI
 */
@Component("orderService")
@Scope("prototype")
public class OrderService {
}
