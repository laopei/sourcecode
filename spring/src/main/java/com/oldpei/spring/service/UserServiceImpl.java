package com.oldpei.spring.service;

import com.oldpei.spring.*;

/**
 * @author OLDPEI
 */
@Component("userService")
//@Scope("prototype")
public class UserServiceImpl implements UserService, BeanNameAware, InitializingBean {
    //    双重校验的单例模式
    private static volatile UserServiceImpl userServiceImpl;

    public static UserServiceImpl getInstance() {
        if (userServiceImpl == null) {
            synchronized (UserServiceImpl.class) {
                if (userServiceImpl == null) {
                    userServiceImpl = new UserServiceImpl();
                }

            }

        }
        return userServiceImpl;
    }

    @Autowired
    private OrderService orderService;

    private String beanName;

    private String name;

    @Override
    public void test() {
        System.out.println("原方法 调用test方法");
//        System.out.println(orderService);
//        System.out.println("beanName:" + beanName);
    }

    @Override
    public void test1() {
        System.out.println("原方法 调用test1方法");
//        System.out.println(orderService);
//        System.out.println("beanName:" + beanName);
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("初始化");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
