package com.oldpei.spring.service;

import com.oldpei.spring.BeanPostProcessor;
import com.oldpei.spring.Component;

import java.lang.reflect.Proxy;

/**
 * @author OLDPEI
 * bean创建时的后置处理器的实现
 */
@Component
public class OldpeiBeanPostProcessor implements BeanPostProcessor {
    /**
     * 初始化之前操作
     *
     * @param bean
     * @param beanName
     * @return
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println("初始化前");
        if (beanName.equals("userService")) {
            ((UserServiceImpl) bean).setName("postProcessBeforeInitialization");
        }

        return bean;
    }

    /**
     * 初始化之后操作
     *
     * @param bean
     * @param beanName
     * @return
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("初始化后");
        //切面实现逻辑判断
        // spring aop实现方案，代理对象实现

        if (beanName.equals("userService")) {
            ClassLoader classLoader = OldpeiBeanPostProcessor.class.getClassLoader();
            Class<?>[] interfaces = bean.getClass().getInterfaces();
            Object proxyInstance = Proxy.newProxyInstance(classLoader, interfaces, (proxy, method, args) -> {
                //先执行代理逻辑
                System.out.println("代理方法 " + method.getName());
                //在执行业务逻辑
                return method.invoke(bean, args);
            });
            return proxyInstance;
        }
        return bean;
    }
}
